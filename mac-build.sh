OPENCV_DIR=/Users/buskysnow/jerome/projects/opencv-3.4.7/opencv3
LIB_DIR=/Users/buskysnow/jerome/projects/paddle
CUDA_LIB_DIR=/usr/local/cuda/lib64
CUDNN_LIB_DIR=/usr/lib/x86_64-linux-gnu/

BUILD_DIR=build
rm -rf ${BUILD_DIR}
mkdir ${BUILD_DIR}
cd ${BUILD_DIR}
cmake .. \
    -DPADDLE_LIB=${LIB_DIR} \
    -DWITH_MKL=OFF \
    -DWITH_GPU=OFF \
    -DWITH_STATIC_LIB=OFF \
    -DWITH_TENSORRT=OFF \
    -DOPENCV_DIR=${OPENCV_DIR} \
    # -DCMAKE_C_COMPILER=/usr/local/bin/gcc \
    # -DCMAKE_CXX_COMPILER=/usr/local/bin/g++ \

make VERBOSE=1 -j4
