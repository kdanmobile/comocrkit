// Copyright (c) 2020 PaddlePaddle Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



#include <iostream>
#include <include/ocr_api.h>
#include <include/server.h>

#include <locale.h>
#include <iostream>
#include <string>

#include <ctime>
#include <ratio>
#include <chrono>

#include <chrono>
#include <thread>




using namespace std;
using namespace std::chrono;

//std::string WCTOS(const wchar_t* src) {
//    if (!src) {
//        return nullptr;
//    }
//    size_t b_size = wcslen(src);
//    char* csrc = (char*)malloc((b_size + 1) * sizeof(char));
//    wcstombs(csrc, src, b_size);
//    csrc[b_size] = '\0';
//    return std::string(csrc);
//}
//
//wstring  STOWC(const char* src) {
//    if (!src) {
//        return NULL;
//    }
//    size_t b_size = strlen(src);
//    wchar_t* dst = new wchar_t[b_size];
//    wmemset(dst, 0, b_size);
//    mbstowcs(dst, src, b_size);
//
//    //std::wcout << wstring(dst) << std::endl;
//    wstring ret = wstring(dst);
//    delete dst;
//    return ret;
//
//}



void ocr_callback(long id, bool is_success, const char* result, float integrated_score) {
    std::cout << "integrated_score:" << integrated_score << std::endl;
    if (is_success) {
        std::cout << std::string(result) << std::endl;
    }
   

}


//void GetMemory(char** p)
//{
//    *p = (char*)malloc(sizeof(char) * 100);
//    strcpy(*p, "hello world");
//}


int main(int argc, char **argv) {
    


#ifdef _UNIX
    auto ocrServer = std::make_shared<OCRServer>(8199);
    ocrServer->Run();
    
    while(true){
        std::this_thread::sleep_for(std::chrono::milliseconds(10000));      
    }
#else
    Init("/Users/buskysnow/jerome/projects/ch_PP-OCRv2_rec_infer",
         "/Users/buskysnow/jerome/projects/ch_PP-OCRv2_det_infer",
         "", 
         "/Users/buskysnow/jerome/projects/ppocr_keys_v1.txt");

    //std::this_thread::sleep_for(std::chrono::milliseconds(5000));
  /* SetFloatParams(PARAMS::BOX_THRESH, 0.3);
    SetFloatParams(PARAMS::UNCLIP_RATIO, 1.6);
    SetIntParams(PARAMS::MAX_SIDE_LEN, 960);
    SetIntParams(PARAMS::BATCH_NUMBER, 1);
    SetIntParams(PARAMS::CPU_THREADS,2);
    SetBoolParams(PARAMS::ENABLE_MKLDNN, true);*/
    RegisterCallback(&ocr_callback);
    // SetIntParams(PARAMS::MAX_SIDE_LEN, 2240);
//SetIntParams(PARAMS::CPU_THREADS,10);
    //CKO_CHAR* path = L"D:\\paddle\\PaddleOCR\\deploy\\cpp_infer\\out\\build\\x64-Release\\Release\\images\\21.png";
    //char* path1 = "D:\\paddle\\PaddleOCR\\deploy\\cpp_infer\\out\\build\\x64-Release\\Release\\images\\2.png";
    int count = 0;
//    while (true) {
        char* path1 = "/Users/buskysnow/jerome/projects/转档流程.jpeg";
        ////
        //std::this_thread::sleep_for(std::chrono::milliseconds(5000));
        //
        ////long id = Prepare(path);
        long id1 = Prepare(path1);
        ////long id2 = Prepare(path);
        auto t1 = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
        ////Recognize(id);
        Recognize(id1);
        //std::this_thread::sleep_for(std::chrono::milliseconds(5000));
        auto t2 = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::system_clock::now());
        auto time_span = t2 - t1;
        std::cout << "recognize time:" << time_span.count() << std::endl;
        std::cout << "recognize count:" << (++count) << std::endl;
//    }



    char* str = NULL;
    //GetMemory(&str);
    
    GetVersion(&str);

    std::cout << "version : " << std::string(str) << std::endl;
    free(str);
    


    // Destory();
#endif
    //std::this_thread::sleep_for(std::chrono::milliseconds(10000));
    return 0;
}


