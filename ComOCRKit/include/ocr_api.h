#pragma once

#ifndef COMPDFKIT_OCR_H
#define COMPDFKIT_OCR_H

#ifdef __cplusplus
extern "C" {
#endif 


#if defined(_MSC_VER)
#define ComOCRKitLib_API __declspec(dllexport)
#else
#define ComOCRKitLib_API 
#endif

typedef enum {
	BOX_THRESH,//The threshold value of the DB post-processing filter box, if there is a missing box in the detection, it can be reduced as appropriate
	UNCLIP_RATIO,//The compactness of the text box, the smaller the text box, the closer to the text
	MAX_SIDE_LEN,//When the length and width of the input image are greater than the set size, the image is scaled proportionally
	BATCH_NUMBER,//The number of recognitions at one time affects the recognition efficiency
	ENABLE_MKLDNN,//Whether use mkldnn with CPU.
	CPU_THREADS, //Num of threads with CPU affects the recognition efficiency.
}PARAMS;


typedef void(*com_ocr_callback)(long id,bool is_success,const char* result,float integrated_score);
typedef void(*com_ocr_log_callback)(const char* result);

ComOCRKitLib_API bool Init(char* rec_model_path, char* det_model_path, char* cls_model_path, char* dict_path);
ComOCRKitLib_API void Destory();
ComOCRKitLib_API long Prepare(const char* image_path);

/**
The value of BOX_THRESH is between 0.3 and 0.6 the default is 0.3
The value of UNCLIP_RATIO is between 1.5 and 2.0 ,the default is 1.6
**/
ComOCRKitLib_API bool SetFloatParams(PARAMS param,float value);

/**
The value of ENABLE_MKLDNN is BOOL value , the default is true
**/
ComOCRKitLib_API bool SetBoolParams(PARAMS param, bool value);


/**
The value of MAX_SIDE_LEN must be a multiple of 320 , the default is 1920
The value of BATCH_NUMBER is between 1 and 20 , the default is 6
The value of CPU_THREADS is between 1 and 10 , the default is 2
**/
ComOCRKitLib_API bool SetIntParams(PARAMS param, int value);


ComOCRKitLib_API bool Recognize(long id);
ComOCRKitLib_API void RegisterCallback(com_ocr_callback callback);
ComOCRKitLib_API void RegisterLogCallback(com_ocr_log_callback callback);
ComOCRKitLib_API bool GetVersion(char** version);

#ifdef __cplusplus
}  // extern "C"
#endif
#endif