﻿
#include <include/ocr_api.h>

#include "glog/logging.h"
#ifndef _APPLE
#include "omp.h"
#endif
#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <chrono>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <vector>

#include <cstring>
#include <fstream>
#include <numeric>

#include <glog/logging.h>
#include <include/ocr_det.h>
#include <include/ocr_cls.h>
#include <include/ocr_rec.h>
#include <include/utility.h>
#include <sys/stat.h>

#include <gflags/gflags.h>
#include "include/auto_log/autolog.h"
#include <cstdlib>

#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"


#include <locale.h>
#include <iostream>
#include <string>


#include <unordered_map>
#include <mutex>

#include "httplib.h"
#include "include/ByteBuffer.h"


using namespace rapidjson;
using namespace PaddleOCR;
DEFINE_bool(use_gpu, false, "Infering with GPU or CPU.");
DEFINE_int32(gpu_id, 0, "Device id of GPU to execute.");
DEFINE_int32(gpu_mem, 4000, "GPU id when infering with GPU.");
DEFINE_int32(cpu_threads, 2, "Num of threads with CPU.");
DEFINE_bool(enable_mkldnn, true , "Whether use mkldnn with CPU.");
DEFINE_bool(use_tensorrt, false, "Whether use tensorrt.");
DEFINE_string(precision, "fp32", "Precision be one of fp32/fp16/int8");
//DEFINE_bool(benchmark, false, "Whether use benchmark.");
DEFINE_string(save_log_path, "./log_output/", "Save benchmark log path.");
// detection related
DEFINE_string(image_dir, "", "Dir of input image.");
DEFINE_string(det_model_dir, "./ch_en/ch_PP-OCRv2_det_slim_quant_infer", "Path of det inference model.");
DEFINE_int32(max_side_len, 960, "max_side_len of input image.");
DEFINE_double(det_db_thresh, 0.3, "Threshold of det_db_thresh.");
DEFINE_double(det_db_box_thresh, 0.5, "Threshold of det_db_box_thresh.");
DEFINE_double(det_db_unclip_ratio,1.6, "Threshold of det_db_unclip_ratio.");
DEFINE_bool(use_polygon_score, false, "Whether use polygon score.");
DEFINE_bool(use_dilation, true, "Whether use the dilation on output map.");
DEFINE_bool(visualize, false, "Whether show the detection results.");
// classification related
DEFINE_bool(use_angle_cls, false, "Whether use use_angle_cls.");
DEFINE_string(cls_model_dir, "", "Path of cls inference model.");
DEFINE_double(cls_thresh, 0.9, "Threshold of cls_thresh.");
// recognition related
DEFINE_string(rec_model_dir, "./ch_en/ch_PP-OCRv2_rec_slim_quant_infer", "Path of rec inference model.");
DEFINE_int32(rec_batch_num, 6, "rec_batch_num.");
DEFINE_string(char_list_file, "./ch_en/ppocr_keys_v1.txt", "Path of dictionary.");


com_ocr_callback ocr_callback;

std::unordered_map<long, std::string> umap;
long id_count = 0;
std::mutex mtx;


Classifier* cls = nullptr;
DBDetector* det = nullptr;
CRNNRecognizer* rec = nullptr;

std::string VERSION = "alpha_V1.0.1_2103291423";
int VERSION_CODE = 2103250947;



void ocr_listen(long id, bool is_success, const char* result, float integrated_score) {
    if (ocr_callback) {
        ocr_callback(id,is_success,result, integrated_score);
    }
}

void InitOCRComponent(bool isRec,bool isDet,bool isCls) {
    if (isRec) {
        if (rec) {
            delete rec;
        }

        std::string char_list_file = FLAGS_char_list_file;
        rec = new CRNNRecognizer(FLAGS_rec_model_dir, FLAGS_use_gpu, FLAGS_gpu_id,
            FLAGS_gpu_mem, FLAGS_cpu_threads,
            FLAGS_enable_mkldnn, char_list_file,
            FLAGS_use_tensorrt, FLAGS_precision, FLAGS_rec_batch_num);
    }

    if (isDet) {
        if (det) {
            delete det;
        }

        det = new DBDetector(FLAGS_det_model_dir, FLAGS_use_gpu, FLAGS_gpu_id,
            FLAGS_gpu_mem, FLAGS_cpu_threads,
            FLAGS_enable_mkldnn, FLAGS_max_side_len, FLAGS_det_db_thresh,
            FLAGS_det_db_box_thresh, FLAGS_det_db_unclip_ratio,
            FLAGS_use_polygon_score, FLAGS_use_dilation, FLAGS_visualize,
            FLAGS_use_tensorrt, FLAGS_precision);
    }

    if (isCls) {
        if (cls) {
            delete cls;
        }

        if (FLAGS_use_angle_cls) {
            cls = new Classifier(FLAGS_cls_model_dir, FLAGS_use_gpu, FLAGS_gpu_id,
                FLAGS_gpu_mem, FLAGS_cpu_threads,
                FLAGS_enable_mkldnn, FLAGS_cls_thresh,
                FLAGS_use_tensorrt, FLAGS_precision);
        }
    }
}

ComOCRKitLib_API bool Init(char* rec_model_path, char* det_model_path, char* cls_model_path, char* dict_path) {

    FLAGS_det_model_dir = std::string(det_model_path);
    FLAGS_rec_model_dir = std::string(rec_model_path);
    FLAGS_cls_model_dir = std::string(cls_model_path);

    FLAGS_char_list_file = std::string(dict_path);

    InitOCRComponent(true, true, true);
    
    return true;
}

ComOCRKitLib_API void Destory()
{
    if (cls) {
        delete cls;
    }

    if (rec) {
        delete rec;
    }

      if (cls) {
        delete cls;
    }
}

ComOCRKitLib_API bool SetFloatParams(PARAMS param, float value) {
    if (!param) {
        return false;
    }

    if (param == PARAMS::BOX_THRESH && value >= 0.3 && value <= 0.6) {
        FLAGS_det_db_box_thresh = value;
        InitOCRComponent(false, true, false);
    }
    else if (param == PARAMS::UNCLIP_RATIO && value >= 1.5 && value <= 2.0) {
        FLAGS_det_db_unclip_ratio = value;
        InitOCRComponent(false, true, false);
    } else {
        std::cout << " ----------------------SetParams Failed-------------------- : " << std::endl;
        return false;
    }
    return true;
}

ComOCRKitLib_API bool SetBoolParams(PARAMS param, bool value)
{
    if (param == PARAMS::ENABLE_MKLDNN) {
        FLAGS_enable_mkldnn = value;
        InitOCRComponent(true, true, false);
    }
    else {
        return false;
    }

    return true;
}

ComOCRKitLib_API bool SetIntParams(PARAMS param, int value)
{
    if (param == PARAMS::MAX_SIDE_LEN && ((((int)value) % 320) == 0)) {
        FLAGS_max_side_len = value;
        InitOCRComponent(false, true, false);
    }
    else if (param == PARAMS::BATCH_NUMBER && (value > 1)) {
        FLAGS_rec_batch_num = value;
        InitOCRComponent(true, false, false);
    }
    else if (param == PARAMS::CPU_THREADS && (value >= 1) && (value <= 10)) {
        FLAGS_cpu_threads = value;
        InitOCRComponent(true, true, false);
    }
    else {
        return false;
    }

    return true;
}

ComOCRKitLib_API long Prepare(const char* image_path) {
    if (umap.size() > 1000) {
        for (int i = 0; i < 500; i++) {
            umap.erase(umap.begin());
        }
    }
    //std::cout << "umap.size():" << umap.size() << std::endl;
    std::lock_guard<std::mutex> lock(mtx);
    std::string path = std::string(image_path);
    id_count++;
    umap.insert({id_count,path});
    return id_count;

}

ComOCRKitLib_API bool Recognize(long id)
{   

    if (!rec || !det) {
        return false;
    }

    //CKO_CHAR* path1 = L"D:\\2.png";
    std::cout << FLAGS_det_model_dir << std::endl;

    std::string path = umap[id];
    if (path.empty()) {
        return false;
    }
    
    cv::String img_name = cv::String(std::string(path));
    cv::Mat srcimg = cv::imread(img_name, cv::IMREAD_COLOR);
    if (!srcimg.data) {
        std::cerr << "[ERROR] image read failed! image path: " << img_name << endl;
        return false;
    }

    int srcimg_width = srcimg.cols;
    int srcimg_height = srcimg.rows;
    cv::Mat sm;

    /*if (srcimg_width * srcimg_height < 20000) {
        FLAGS_max_side_len = 960;
    }
    else {
        FLAGS_max_side_len = 2280;
    }*/

    srcimg.copyTo(sm);
    std::cout << "srcimg_width : " << srcimg_width << " || srcimg_height :  " << srcimg_height << std::endl;
    float radio = 0.0;
    if (srcimg_width < 1920) {
        radio = 1920 / srcimg_width;
        std::cout << "radio : " << radio << std::endl;
        cv::resize(srcimg, srcimg, cv::Size(), radio, radio);
    }

    //float radio = 0.0;

    std::vector<std::vector<std::vector<int>>> boxes;

    std::vector<std::vector<std::vector<int>>> sm_boxes;
    std::vector<double> det_times;
    std::vector<double> rec_times;

    std::vector<double> time_info_det = { 0, 0, 0 };
    std::vector<double> time_info_rec = { 0, 0, 0 };
    
    det->Run(srcimg, boxes, &det_times);
    time_info_det[0] += det_times[0];
    time_info_det[1] += det_times[1];
    time_info_det[2] += det_times[2];

    std::vector<cv::Mat> img_list;
    for (int j = 0; j < boxes.size(); j++) {
        cv::Mat crop_img;
        crop_img = Utility::GetRotateCropImage(srcimg, boxes[j]);
        std::vector<std::vector<int>> layer2;

        for (auto ita = boxes[j].begin(); ita != boxes[j].end(); ++ita) {
            std::vector<int> layer1;
            for (auto it = ita->begin(); it != ita->end(); ++it) {
                if (radio == 0.0) {
                    radio = 1;
                }
                float v = *it / radio;
                //std::cout << *it << " | " << v << "  |  ";

                layer1.push_back((int)v);
            }
            layer2.push_back(layer1);
            //std::cout << "\n";
        }

        sm_boxes.push_back(layer2);

        if (cls != nullptr) {
            crop_img = cls->Run(crop_img);
        }
        //std::string filename;
        //filename.append(std::to_string(j)).append(".png");
        //cv::imwrite(filename, crop_img);
        img_list.push_back(crop_img);

    }

    //Utility::VisualizeBboxes(sm, sm_boxes);

    std::vector<std::string> text_array;
    std::vector<float> score_array;
    std::vector<int> failed_indexes;
    float integrated_score;
    rec->Run(img_list, &rec_times , &text_array, &score_array,&integrated_score,&failed_indexes);

    //remove failed boxes
    for (int i = 0; i < failed_indexes.size(); i++) {
        if (failed_indexes[i] < sm_boxes.size()) {
            sm_boxes.erase(sm_boxes.begin() + failed_indexes[i]);
        }
    }

    if (isnan(integrated_score)) {
        integrated_score = 0;
    }
    std::cout << "text_array size : " << text_array.size() << " , sm_boxes : " << sm_boxes.size() << " , score : " << score_array.size() << std::endl;
    if (text_array.size() == score_array.size()) {

        size_t size = std::min(text_array.size(), sm_boxes.size());
        size = std::min(size, score_array.size());


        Document doc;
        doc.SetObject();
        rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();

        doc.AddMember("width", srcimg_width, allocator);
        doc.AddMember("height", srcimg_height, allocator);
        doc.AddMember("integrated_score", integrated_score, allocator);
        doc.AddMember("line_size", static_cast<int>(size), allocator);
        Value lines(rapidjson::kArrayType);
        
        for (int i = 0; i < size; i++) {
            Value lines_object(rapidjson::kObjectType);
            lines_object.SetObject();
            lines_object.AddMember("text", text_array[i], allocator);
            lines_object.AddMember("score", score_array[i], allocator);

            rapidjson::Value position_array(rapidjson::kArrayType);
            for (auto ita = sm_boxes[i].begin(); ita != sm_boxes[i].end(); ++ita) {          
                for (auto it = ita->begin(); it != ita->end(); ++it) {
                    rapidjson::Value position_object(rapidjson::kObjectType);
                    position_object.SetObject();
                    position_object.SetInt(*it);
                    position_array.PushBack(position_object, allocator);
                }            
            }
            lines_object.AddMember("position", position_array, allocator);
            lines.PushBack(lines_object, allocator);
        }
        doc.AddMember("lines", lines, allocator);



        StringBuffer buffer;
        Writer<StringBuffer> writer(buffer);
        doc.Accept(writer);
        //std::cout << buffer.GetString() << std::endl;
        
         //wstring ret_w = STOWC(buffer.GetString());
         //std::cout << WCTOS(ret_w.c_str()) << std::endl;


        ocr_listen(id,true, buffer.GetString(), integrated_score);
        return true;
    }

    return false;
    
}

ComOCRKitLib_API void RegisterCallback(com_ocr_callback callback)
{
    ocr_callback = callback;
}

ComOCRKitLib_API bool GetVersion(char** version)
{   
 
    
    int len = strlen(VERSION.c_str());
    //std::cout << "sdk version:" << VERSION  << " | len:" << (len * sizeof(char) + 1) << std::endl;
    *version = (char*)malloc((len * sizeof(char) + 1));
    if (version) {
        strcpy(*version, VERSION.c_str());
        return true;
    }
    else {
        return false;
    }
    
  /*  std::cout << "sdk version:" << VERSION << " | len:" << (len * sizeof(char) + 1) << std::endl;
    memcpy(version, VERSION.c_str(), len);
    std::cout << "sdk version:" << VERSION << " | len:" << (len * sizeof(char) + 1) << std::endl;
   version[len] = '\0';
   std::cout << "sdk version:" << VERSION << " | len:" << (len * sizeof(char) + 1) << std::endl;*/
    //version = VERSION.c_str();
}




