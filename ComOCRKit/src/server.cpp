#include "include/server.h"
#include "include/ocr_api.h"
#include <string>
#include "httplib.h"
using namespace httplib;
Server svr;


OCRServer::OCRServer(int port){

svr.set_keep_alive_max_count(100); // Default is 5
svr.set_keep_alive_timeout(10);  // Default is 5

svr.set_read_timeout(10, 0); // 5 seconds
svr.set_write_timeout(10, 0); // 5 seconds

        Init("/share/model/ch_ppocr_server_v2.0_rec_infer",
         "/share/model/ch_ppocr_server_v2.0_det_infer",
         "", 
         "/share/model/ch_en.txt");

    //std::this_thread::sleep_for(std::chrono::milliseconds(5000));
//    SetFloatParams(PARAMS::BOX_THRESH, 0.3);
//     SetFloatParams(PARAMS::UNCLIP_RATIO, 1.6);
//     SetIntParams(PARAMS::MAX_SIDE_LEN, 960);
//     SetIntParams(PARAMS::BATCH_NUMBER, 1);
SetIntParams(PARAMS::CPU_THREADS,10);
SetBoolParams(PARAMS::ENABLE_MKLDNN, true);

m_port = port;



}

void OCRServer::Run(){
    svr.Post("/ocr", [&](const auto& req, auto& res) {
        
        bool ret = req.has_file("image");
        if(ret){
            auto size = req.files.size();
            const auto& file = req.get_file_value("image");
            std::cout << "file.filename:" << file.filename << ",file.content_type:" << file.content_type
            << ", size:" << file.content.length() << std::endl;
            
            FILE* pFile;
            pFile = fopen(file.filename.c_str(), "wb");
           
            fwrite(file.content.c_str(), file.content.length(), 1, pFile);
            fclose(pFile);
            long id1 = Prepare(file.filename.c_str());
            
            std::string ret = Recognize(id1);
            if(ret.compare("NULL") != 0){
                res.set_content(ret, "application/json");
            }else {
                res.set_content("error", "text/plain");
            }
            
        }

        // file.filename;
        // file.content_type;
        // file.content;
    });

    svr.listen("0.0.0.0", 8199);

}